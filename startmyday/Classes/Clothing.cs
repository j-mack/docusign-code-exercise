﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace startmyday.Classes
{
	abstract class Clothing
	{
		public Clothing(Dictionary<Weather, string> style)
		{
			Style = style;
		}

		protected Dictionary<Weather, string> Style { get; set; }

		abstract public string Wear(List<Clothing> wardrobe, Weather temperature);

		protected bool DoesWardrobeContain(List<Clothing> wardrobe, Type type)
		{
			// Dynamic way to call wardrobe.OfType<type>().Any();
			MethodInfo method = typeof(Queryable).GetMethod("OfType");
			MethodInfo generic = method.MakeGenericMethod(new Type[] { type });
			return ((IEnumerable<object>)generic.Invoke(null, new object[] { wardrobe.AsQueryable() })).Any();
		}
	}
}
