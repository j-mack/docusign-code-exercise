﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Pajamas : Clothing
	{
		public Pajamas() : base(null) { }

		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			throw new WardrobeMalfunctionException("Pajamas: You shouldn't be putting Pajamas on, it's the start of the day!");
		}
	}
}
