﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Socks : Clothing
	{
		public Socks() : base(new Dictionary<Weather, string>()
		{
			{ Weather.COLD, "socks" }
		}) { }
		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			if (wardrobe == null)									throw new WardrobeMalfunctionException("Socks: Wardrobe must exist");
			if (weather == Weather.HOT)								throw new WardrobeMalfunctionException("Socks: NO SOCKS AND SANDALS");
			if (DoesWardrobeContain(wardrobe, typeof(Pajamas)))		throw new WardrobeMalfunctionException("Socks: Cannot be wearing pajamas");
			if (DoesWardrobeContain(wardrobe, typeof(Socks)))		throw new WardrobeMalfunctionException("Socks: Cannot already be wearing socks");
			if (DoesWardrobeContain(wardrobe, typeof(Footwear)))	throw new WardrobeMalfunctionException("Socks: Cannot put socks on over shoes");

			return Style[weather];
		}
	}
}
