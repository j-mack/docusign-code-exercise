﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Shirt : Clothing
	{
		public Shirt() : base(new Dictionary<Weather, string>()
		{
			{ Weather.HOT,	"t-shirt" },
			{ Weather.COLD,	"shirt" }
		}) { }

		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			if (wardrobe == null)									throw new WardrobeMalfunctionException("Shirt: Wardrobe must exist");
			if (DoesWardrobeContain(wardrobe, typeof(Pajamas)))		throw new WardrobeMalfunctionException("Shirt: Cannot be wearing pajamas");
			if (DoesWardrobeContain(wardrobe, typeof(Shirt)))		throw new WardrobeMalfunctionException("Shirt: Cannot already be wearing a shirt");
			if (DoesWardrobeContain(wardrobe, typeof(Jacket)))		throw new WardrobeMalfunctionException("Shirt: Cannot put a shirt on over a jacket");
			if (DoesWardrobeContain(wardrobe, typeof(Headwear)))	throw new WardrobeMalfunctionException("Shirt: Cannot put a shirt on over a hat");
			
			return Style[weather];
		}
	}
}
