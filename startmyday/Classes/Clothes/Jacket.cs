﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Jacket : Clothing
	{
		public Jacket() : base(new Dictionary<Weather, string>()
		{
			{ Weather.COLD, "jacket" }
		}) { }

		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			if (wardrobe == null)								throw new WardrobeMalfunctionException("Jacket: Wardrobe must exist");
			if (weather == Weather.HOT)							throw new WardrobeMalfunctionException("Jacket: Cannot wear jacket in hot weather");
			if (DoesWardrobeContain(wardrobe, typeof(Pajamas)))	throw new WardrobeMalfunctionException("Jacket: Cannot be wearing Pajamas");
			if (DoesWardrobeContain(wardrobe, typeof(Jacket)))	throw new WardrobeMalfunctionException("Jacket: Cannot already be wearing a jacket");

			return Style[weather];
		}
	}
}
