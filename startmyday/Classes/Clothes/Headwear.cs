﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Headwear : Clothing
	{
		public Headwear() : base(new Dictionary<Weather, string>()
		{
			{ Weather.HOT,	"sun visor" },
			{ Weather.COLD,	"hat" }
		}) { }

		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			if (wardrobe == null)									throw new WardrobeMalfunctionException("Headwear: Wardrobe must exist");
			if (DoesWardrobeContain(wardrobe, typeof(Pajamas)))		throw new WardrobeMalfunctionException("Headwear: Cannot be wearing Pajamas");
			if (DoesWardrobeContain(wardrobe, typeof(Headwear)))	throw new WardrobeMalfunctionException("Headwear: Cannot already be wearing headwear");
			
			return Style[weather];
		}
	}
}
