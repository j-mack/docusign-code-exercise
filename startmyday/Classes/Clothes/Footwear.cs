﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Footwear : Clothing
	{
		public Footwear() : base(new Dictionary<Weather, string>() {
			{ Weather.HOT,	"sandals" },
			{ Weather.COLD,	"boots" }
		}) { }

		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			if (wardrobe == null)									throw new WardrobeMalfunctionException("Footwear: Wardrobe must exist");
			if (DoesWardrobeContain(wardrobe, typeof(Pajamas)))		throw new WardrobeMalfunctionException("Footwear: Cannot be wearing Pajamas");
			if (DoesWardrobeContain(wardrobe, typeof(Footwear)))	throw new WardrobeMalfunctionException("Footwear: Cannot already be wearing shoes");
			
			return Style[weather];
		}
	}
}
