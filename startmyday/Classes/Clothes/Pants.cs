﻿using System.Collections.Generic;

namespace startmyday.Classes.Clothes
{
	class Pants : Clothing
	{
		public Pants() : base(new Dictionary<Weather, string>()
		{
			{ Weather.HOT,	"shorts" },
			{ Weather.COLD,	"pants" }
		}) { }

		public override string Wear(List<Clothing> wardrobe, Weather weather)
		{
			if (wardrobe == null)									throw new WardrobeMalfunctionException("Pants: wardrobe must exist");
			if (DoesWardrobeContain(wardrobe, typeof(Pajamas)))		throw new WardrobeMalfunctionException("Pants: Cannot be wearing Pajamas");
			if (DoesWardrobeContain(wardrobe, typeof(Pants)))		throw new WardrobeMalfunctionException("Pants: Cannot already be wearing pants");
			if (DoesWardrobeContain(wardrobe, typeof(Footwear)))	throw new WardrobeMalfunctionException("Pants: Cannot put pants on over shoes");

			return Style[weather];
		}
	}
}
