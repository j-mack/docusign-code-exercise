﻿using System;

namespace startmyday.Classes
{
	class WardrobeMalfunctionException : Exception
	{
		public WardrobeMalfunctionException() { }
		public WardrobeMalfunctionException(string message) : base(message) { }
		public WardrobeMalfunctionException(string message, Exception inner) : base(message, inner) { }
	}
}
