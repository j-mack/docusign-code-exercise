﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using startmyday.Classes.Clothes;

namespace startmyday.Classes
{
	class Human
	{
		public Human(Weather weather)
		{
			Weather		= weather;
			Clothes		= new List<Clothing>();
			_myDay		= new List<String>();

			// Start the day at home, in your pajamas
			AtHome		= true;
			Clothes.Add(new Pajamas());
		}

		#region Methods
		public void PutOn(Clothing item)
		{
			_myDay.Add(item.Wear(Clothes, Weather));
			Clothes.Add(item);
		}
		public void TakeOff(Clothing item)
		{
			// You can only take off your Pajamas!
			if (item.GetType() != typeof(Pajamas))
				throw new WardrobeMalfunctionException("TakeOff: Only pajamas can be taken off");

			foreach (var clothing in Clothes)
			{
				if (clothing.GetType() == typeof(Pajamas))
				{
					_myDay.Add("Removing PJs");
					Clothes.Remove(clothing);
					break;
				}
			}
		}
		public void LeaveMyHouse()
		{
			if (!AtHome)						throw new WardrobeMalfunctionException("Leaving: Already outside");
			if (AmIWearing(typeof(Pajamas)))	throw new WardrobeMalfunctionException("Leaving: Cannot leave in your pajamas");

			// Requires all clothes for weather except pajamas
			if (Weather == Weather.HOT && (!AmIWearing(typeof(Footwear)) || !AmIWearing(typeof(Headwear)) || !AmIWearing(typeof(Shirt)) || !AmIWearing(typeof(Pants))))
				throw new WardrobeMalfunctionException("Leaving: Missing hot weather clothes");
			
			if (Weather == Weather.COLD && (!AmIWearing(typeof(Footwear)) || !AmIWearing(typeof(Headwear)) || !AmIWearing(typeof(Shirt)) || !AmIWearing(typeof(Shirt)) || !AmIWearing(typeof(Jacket)) || !AmIWearing(typeof(Pants))))
				throw new WardrobeMalfunctionException("Leaving: Missing cold weather clothes");

			// Leave the house!
			AtHome = false;
			_myDay.Add("leaving house");
		}
		#endregion

		#region Helpers
		private bool AmIWearing(Type type)
		{
			// Dynamic way to call Clothing.OfType<T>().Any();
			MethodInfo method = typeof(Queryable).GetMethod("OfType");
			MethodInfo generic = method.MakeGenericMethod(new Type[]{ type });
			return ((IEnumerable<object>)generic.Invoke(null, new object[] { Clothes.AsQueryable() })).Any();
		}
		#endregion

		#region Properties
		public bool AtHome					{ get; set; }
		public Weather Weather				{ get; set; }
		public List<Clothing> Clothes		{ get; set; }

		private List<string> _myDay			{ get; set; }
		public string MyDay	{ get { return String.Join(", ", _myDay.ToArray()); } }
		#endregion
	}
}
