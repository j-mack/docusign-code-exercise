﻿using System;
using System.Collections.Generic;
using startmyday.Classes;
using startmyday.Classes.Clothes;

namespace startmyday
{
	class Program
	{
		static void Main(string[] args)
		{
			// Get input
			// split on space between HOT/COLD and action numbers
			Console.Write("I: ");
			var input = Console.ReadLine().Split(new[] {' '}, 2);
			
			// If we don't have two halves, break
			if (input.Length != 2)
			{
				Console.Write("fail");
				return;
			}

			// Convert string actions into int list
			var day = new List<int>();
			foreach(var action in input[1].Split(new[] {','}))
				day.Add(Int32.Parse(action.Trim()));
			
			Human human = null;

			try
			{
				// Generate our Human based on the weather
				if (input[0] != "HOT" && input[0] != "COLD")
					throw new WardrobeMalfunctionException("Invalid weather type");

				human = new Human(input[0] == "HOT" ? Weather.HOT : Weather.COLD);

				// Process our day
				foreach (int task in day)
				{
					switch(task)
					{
						case 1:		human.PutOn(new Footwear());	break;
						case 2:		human.PutOn(new Headwear());	break;
						case 3:		human.PutOn(new Socks());		break;
						case 4:		human.PutOn(new Shirt());		break;
						case 5:		human.PutOn(new Jacket());		break;
						case 6:		human.PutOn(new Pants());		break;
						case 7:		human.LeaveMyHouse();			break;
						case 8:		human.TakeOff(new Pajamas());	break;
						default:	throw new WardrobeMalfunctionException("Invalid action");
					}
				}

				// Print our day once we're finished
				Console.WriteLine(human.MyDay);
			} catch (WardrobeMalfunctionException) {
				// If we didn't instantiate human, or didn't perform actions, flat fail
				if (human == null || string.IsNullOrWhiteSpace(human.MyDay))
					Console.Write("fail");
				// Otherwise, print actions performed up to failure point
				else
					Console.Write(human.MyDay + ", fail");
			}

			// Wait for end
			Console.ReadKey();
		}
	}
}
